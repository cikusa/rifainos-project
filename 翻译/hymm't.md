# 1 #

Apea'lia Aroshea'safoga!
中秋快乐！

# 2 #

Fee
微风

hyna, ula'idensy'ska walasye't ene
呐，很久很久以前呢 人们认为

ini'fuhunia, grandee'sa fee'kisa
云是微风的妈妈
ank'shea, grandee'la fee'kisa
太阳是微风的爸爸

nave，fee'haf
所以呢 只要有微风的地方
hafe fuhucia
就有软软的天
hafe wuhalir
就有暖暖的阳光

# 3 #

It's time to see what I can do
To test the limits and break through

vit'zani miayasa'co
pace'retes lure en kap